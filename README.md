# SDEFQuality: MARE 2014-19 project - WP4

The aim of this package is to evaluate the conformity of a dataset against a data format description.

It attempt to help on data error detection concerning the data structures, formats, code lists, ...


# Documentation:

Please read here: https://git.outils-is.ird.fr/billet/SDEFQuality/wikis/home

# Installation:

```r
install.packages("devtools")
devtools::install_git('https://git.outils-is.ird.fr/billet/SDEFQuality.git')
```

